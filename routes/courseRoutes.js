const express = require("express");
const router = express.Router();

const {
  addCourse,
  getAllActiveCourses,
  getActiveCourse,
  updateCourse,
  archiveCourse,
} = require("../controllers/courseController");

const { verify } = require("../auth");

router.post("/add", verify, addCourse);

router.get("/active", getAllActiveCourses);

router.get("/:courseId/active", getActiveCourse);

router.put("/:courseId/update", verify, updateCourse);

router.patch("/:courseId/archive", verify, archiveCourse);

module.exports = router;
